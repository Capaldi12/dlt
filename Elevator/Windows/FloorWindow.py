from ..UI import Ui_FloorWindow
from ..Components import Intention, Settings

from .window_mixins import NoCloseMixin, HasElevatorsMixin

from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal


class FloorWindow(QMainWindow, Ui_FloorWindow, HasElevatorsMixin, NoCloseMixin):
    # Signals
    elevator_up_requested: pyqtBoundSignal = pyqtSignal()
    elevator_down_requested: pyqtBoundSignal = pyqtSignal()

    def __init__(self):
        super().__init__()

        self.setupUi(self)

        self.__mixinit__()

        self.up_button.clicked.connect(self.elevator_up_requested)
        self.down_button.clicked.connect(self.elevator_down_requested)

        self.elevator_window_requested.connect(self.on_exit)

    def set_number(self, num):

        num_text = str(num + 1)
        num_text = '0' * (2 - len(num_text)) + num_text

        self.floor_number.setText(num_text)
        self.setWindowTitle(f'Floor {num_text}')

    def open_elevator_doors(self, i_elevator: int):
        self.elevator_displays[i_elevator].open_doors()

    def close_elevator_doors(self, i_elevator: int):
        self.elevator_displays[i_elevator].close_doors()

    def set_up_visible(self, visible: bool = True):
        self.up_button.setVisible(visible)

    def set_down_visible(self, visible: bool = True):
        self.down_button.setVisible(visible)

    def on_exit(self):
        if Settings.close_on_exit:
            self.hide()
