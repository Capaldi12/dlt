from ..Widgets import ElevatorDisplayOutside

from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal
from PyQt5.QtGui import QCloseEvent

from typing import List
from functools import partial


class BaseMixin:
    def __mixinit__(self):
        pass


class NoCloseMixin(BaseMixin):
    def __mixinit__(self):
        super().__mixinit__()

    def closeEvent(self, event: QCloseEvent):
        self.hide()
        event.ignore()


class HasElevatorsMixin(BaseMixin):
    elevator_window_requested: pyqtBoundSignal = pyqtSignal(int)

    elevator_displays: List[ElevatorDisplayOutside]

    def __mixinit__(self):
        super().__mixinit__()

        self.elevator_displays = []

    def add_elevator_display(
            self, out_of: int = 3,
            close_doors: bool = True) -> ElevatorDisplayOutside:

        n = len(self.elevator_displays)

        fd = ElevatorDisplayOutside()

        # This elevator layout should be present in child class
        self.elevator_layout.addWidget(fd, n // out_of, n % out_of)

        fd.button_clicked.connect(partial(self._on_elevator_enter_pressed, n))

        if close_doors:
            fd.close_doors()

        self.elevator_displays.append(fd)

        return fd

    def connect_elevator(self, elevator, i: int):
        elevator.intention_changed.connect(
            self.elevator_displays[i].display
        )

    def _on_elevator_enter_pressed(self, i):
        self.elevator_window_requested.emit(i)


class HasFloorsMixin(BaseMixin):
    floor_requested: pyqtBoundSignal = pyqtSignal(int)

    floor_buttons: List[QPushButton]

    def __mixinit__(self):
        super().__mixinit__()

        self.floor_buttons = []

    def add_floor_button(self, out_of: int = 3) -> QPushButton:
        n = len(self.floor_buttons)

        b = QPushButton(str(n + 1))

        # This elevator layout should be present in child class
        self.floor_layout.addWidget(b, n // out_of, n % out_of)

        self.floor_buttons.append(b)

        b.clicked.connect(partial(self._on_floor_button_pressed, n))

        return b

    def _on_floor_button_pressed(self, i):
        self.floor_requested.emit(i)
