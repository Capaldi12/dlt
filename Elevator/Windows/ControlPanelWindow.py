from .. import app
from ..UI import Ui_ControlPanelWindow

from .window_mixins import HasElevatorsMixin, HasFloorsMixin

from PyQt5.QtWidgets import QMainWindow, QMessageBox, QListWidgetItem
from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal
from PyQt5.QtGui import QCloseEvent


class ControlPanelWindow(QMainWindow, Ui_ControlPanelWindow,
                         HasElevatorsMixin, HasFloorsMixin):

    def __init__(self):
        super().__init__()

        self.setupUi(self)

        self.__mixinit__()

    def closeEvent(self, event: QCloseEvent):
        res = QMessageBox.question(
            self, 'Exit?', 'Are you sure you want to exit?')

        if res == QMessageBox.Yes:
            event.accept()
            app.quit()

        else:
            event.ignore()

    def print_in_console(self, text: str):
        item = QListWidgetItem(text)
        self.console.addItem(item)

        self.console.scrollToItem(item)
