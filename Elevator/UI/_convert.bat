@echo off

..\..\venv\scripts\pyuic5 ControlPanelWindow.ui -o Ui_ControlPanelWindow.py
..\..\venv\scripts\pyuic5 ElevatorWindow.ui -o Ui_ElevatorWindow.py
..\..\venv\scripts\pyuic5 FloorWindow.ui -o Ui_FloorWindow.py
..\..\venv\scripts\pyuic5 ElevatorDisplayOutside.ui -o Ui_ElevatorDisplayOutside.py
