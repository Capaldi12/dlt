from .SystemWithWindow import SystemWithWindow

from ..Windows import ControlPanelWindow
from ..Components import Elevator, Floor, Intention, Settings

from PyQt5.QtCore import QTimer

from typing import List
from functools import partial


class ControlPanel(SystemWithWindow):
    # Components
    window: ControlPanelWindow

    elevators: List[Elevator]
    floors: List[Floor]

    _instance: "ControlPanel" = None

    def __init__(self, parent=None,
                 floor_count: int = 12,
                 elevator_count: int = 3):

        super().__init__(ControlPanelWindow(), parent)

        self._set_instance(self)

        self.elevators = []
        self.floors = []

        for i in range(floor_count):
            floor = Floor(self, number=i, elevator_count=elevator_count,
                          first=(i == 0), last=(i == floor_count - 1))

            self.floors.append(floor)

            self.window.add_floor_button(Settings.floors_per_row)

            self.log('Floor', i + 1, 'created')

            floor.elevator_window_requested.connect(self.open_elevator_window)
            floor.elevator_requested.connect(self.on_elevator_requested)

        for i in range(elevator_count):
            elevator = Elevator(self, number=i,
                                floor_count=floor_count)

            self.elevators.append(elevator)

            self.window.add_elevator_display(Settings.elevators_per_row,
                                             close_doors=False)

            self.log('Elevator', i + 1, 'created')

            elevator.elevator_exited.connect(self.open_floor_window)
            elevator.floor_requested.connect(
                partial(self.on_floor_requested, i))
            elevator.elevator_arrived.connect(
                partial(self.on_elevator_arrived, i))
            elevator.elevator_departed.connect(
                partial(self.on_elevator_departed, i))

            for floor in self.floors:
                floor.connect_elevator(elevator, i)

            self.window.connect_elevator(elevator, i)

            elevator.arrive(0)
            elevator.intention = Intention(0, Intention.STOP)

        self.window.elevator_window_requested.connect(self.open_elevator_window)
        self.window.floor_requested.connect(self.open_floor_window)

    def start(self):
        self.window.show()

        return self

    def open_elevator_window(self, i: int):
        self.elevators[i].show()

    def open_floor_window(self, i: int):
        self.floors[i].show()

    def on_floor_requested(self, i_elevator: int, i_floor: int):
        self.log(f'Elevator {i_elevator + 1} requested '
                 f'to visit floor {i_floor + 1}')

        elevator = self.elevators[i_elevator]

        elevator.depart()

        dir_ = Intention.UP \
            if elevator.current_floor < i_floor \
            else Intention.DOWN

        elevator.intention = Intention(elevator.current_floor, dir_)

        time = abs(elevator.current_floor - i_floor + 1) * 500

        QTimer.singleShot(time, partial(elevator.arrive, i_floor))

    def on_elevator_arrived(self, i_elevator: int, i_floor: int):
        self.log(f'Elevator {i_elevator + 1} arrived on floor {i_floor + 1}')

    def on_elevator_departed(self, i_elevator: int, i_floor: int):
        self.log(f'Elevator {i_elevator + 1} departed from floor {i_floor + 1}')

    def on_elevator_requested(self, intention: Intention):
        self.log(f'Floor {intention.floor + 1} requested '
                 f'an elevator going {intention.dir}')

    def _log(self, *args, sep: str = ' '):
        self.window.print_in_console(sep.join(str(arg) for arg in args))

    @classmethod
    def _set_instance(cls, instance: "ControlPanel"):
        cls._instance = instance

    @classmethod
    def log(cls, *args, sep: str = ' '):
        cls._instance._log(*args, sep=sep)
