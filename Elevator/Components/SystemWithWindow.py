from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import QObject, Qt


class SystemWithWindow(QObject):
    window: QMainWindow

    def __init__(self, window, parent=None):
        super().__init__(parent)

        self.window = window

    def show(self):
        self.window.show()

        self.window.setWindowState(
            int(self.window.windowState())
            & ~Qt.WindowMinimized
            | Qt.WindowActive
        )

        self.window.activateWindow()
