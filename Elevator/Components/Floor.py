from .SystemWithWindow import SystemWithWindow

from ..Windows import FloorWindow
from ..Components import Elevator, Intention, Settings

from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal

from functools import partial


class Floor(SystemWithWindow):
    # Components
    window: FloorWindow

    # Signals
    elevator_window_requested: pyqtBoundSignal = pyqtSignal(int)
    elevator_requested: pyqtBoundSignal = pyqtSignal(Intention)

    # Data
    number: int = 0

    def __init__(self, parent=None, *,
                 number=0, elevator_count=0,
                 first=False, last=False):

        super().__init__(FloorWindow(), parent)

        self.number = number

        self.window.set_number(number)

        self.window.elevator_window_requested.connect(
            self.elevator_window_requested
        )

        self.window.elevator_up_requested.connect(self.on_elevator_up)
        self.window.elevator_down_requested.connect(self.on_elevator_down)

        self.setup_elevators(elevator_count)

        self.window.set_up_visible(not last)
        self.window.set_down_visible(not first)

    def setup_elevators(self, count):
        for i in range(count):
            self.window.add_elevator_display(Settings.elevators_per_row)

    def connect_elevator(self, elevator: Elevator, i: int):
        self.window.connect_elevator(elevator, i)

        elevator.elevator_arrived.connect(
            partial(self.on_arrival, i)
        )

        elevator.elevator_departed.connect(
            partial(self.on_departure, i)
        )

    def on_arrival(self, i_elevator: int, floor: int):
        if floor == self.number:
            self.window.open_elevator_doors(i_elevator)

    def on_departure(self, i_elevator: int, floor: int):
        if floor == self.number:
            self.window.close_elevator_doors(i_elevator)

    def on_elevator_up(self):
        self.elevator_requested.emit(Intention(self.number, Intention.UP))

    def on_elevator_down(self):
        self.elevator_requested.emit(Intention(self.number, Intention.DOWN))
